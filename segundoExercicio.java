package src;
import java.util.Scanner;

public class segundoExercicio {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        
        System.out.println("Calculo de IMC, digite abaixo sua altura: (ex: 1,89)");
        
        double altura = input.nextDouble();
        altura = altura * 2;

        System.out.println("Agora digite seu peso:");
        double peso = input.nextDouble();

        double resultado = peso / altura;

        System.out.println("Seu IMC É: " + resultado);

    }
}
